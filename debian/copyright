Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mmv
Upstream-Contact: Reuben Thomas <rrt@sc3d.org>
Source: https://github.com/rrthomas/mmv

Files: *
Copyright: 1989-1990 Vladimir Lanin <vlad@brm.com>
           2021-2023 Reuben Thomas <rrt@sc3d.org>
License: GPL-3+
Comment:
 The original copyright statement was:
 .
   Mmv is freeware. That means that the entire package of software and
   documentation is copyrighted, and may not be distributed with any
   modifications or for any charge (without the author's explicit written
   permission). Other than that, it may be used and distributed freely.
   .
   Vladimir Lanin
   330 Wadsworth Ave, Apt 6F
   New York, NY 10040
   .
   lanin@csd2.nyu.edu
   ...!cmcl2!csd2!lanin
 .
 Later the original author declared to Debian by e-mail:
 .
   Michael,
   .
   This message is to serve as an announcement that I am changing the
   copyright of mmv to GPL.
   .
   If this message is in any way insufficient to do so, please tell me what
   I have to do. Please keep in mind that I do not have in hand either the
   full GPL text or the source code of the last mmv release (oops).
   .
   If there is any other way that I can help out, please tell me.
   .
   Thanks,
   .
   Vladimir Lanin
   vlad@brm.com
   .
 Thanks to Joost for getting into touch with him.

Files: build-aux/*
Copyright: 2004-2019 Bootstrap Authors
License: MIT or GPL-3+

Files: m4/*
Copyright: 1992-2023 Free Software Foundation, Inc.
License: FSFULLR

Files: lib/*
Copyright: 1987-2023 Free Software Foundation, Inc.
License: LGPL-2.1+

Files: build-aux/config* lib/Makefile.* m4/gnulib-cache.m4 m4/gnulib-comp.m4
Copyright: 1987-2023 Free Software Foundation, Inc.
License: GPL-3+

Files: lib/arg-nonnull.h lib/_Noreturn.h lib/c++defs.h lib/warn-on-use.h
Copyright: 1987-2023 Free Software Foundation, Inc.
License: LGPL-2+

Files: build-aux/mdate-sh
Copyright: 1995-2023 Free Software Foundation, Inc.
License: GPL-2+

Files: build-aux/install-sh
Copyright: 1994 X Consortium
License: X11

Files: debian/*
Copyright: 1996-1998 Michael Meskes <meskes@debian.org>
           1998-2006 Bernd Eckenfels <ecki@debian.org>
           1999      Hartmut Koptein <koptein@debian.org>
           2009-2016 Rhonda D'Vine <rhonda@debian.org>
           2012-2023 Axel Beckert <abe@debian.org>
License: GPL-3+
Comment: Without explicit license statement it is assumed that the
 Debian packaging is under the same license as the upstream code.
 .
 Some of the changes made in Debian have been incomporated upstream.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License, version 2 can be found in
 "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License, version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".

License: FSFULLR
 This file is free software; the Free Software Foundation gives
 unlimited permission to copy and/or distribute it, with or without
 modifications, as long as this notice is preserved.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice (including the
 next paragraph) shall be included in all copies or substantial
 portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: X11
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium
 shall not be used in advertising or otherwise to promote the sale,
 use or other dealings in this Software without prior written
 authorization from the X Consortium.
