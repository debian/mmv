/*
  File autogenerated by gengetopt version 2.23
  generated with the following command:
  gengetopt --unamed-opts 

  The developers of gengetopt consider the fixed text that goes in all
  gengetopt output files to be in the public domain:
  we make no copyright claims on it.
*/

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef FIX_UNUSED
#define FIX_UNUSED(X) (void) (X) /* avoid warnings for unused params */
#endif

#include <getopt.h>

#include "cmdline.h"

const char *gengetopt_args_info_purpose = "move/copy/link multiple files by wildcard patterns";

const char *gengetopt_args_info_usage = "Usage: " CMDLINE_PARSER_PACKAGE " [-m|-x|-r|-c|-o|-a|-l|-s] [-h] [-d|-p] [-g|-t] [-v|-n] FROM TO";

const char *gengetopt_args_info_versiontext = "Copyright (c) 2023 Reuben Thomas <rrt@sc3d.org>.\nCopyright (c) 1990 Vladimir Lanin.\nLicence GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.";

const char *gengetopt_args_info_description = "The FROM pattern is a shell glob pattern, in which `*' stands for any number\nof characters and `?' stands for a single character.\n\nUse #[l|u]N in the TO pattern to get the string matched by the Nth\nFROM pattern wildcard [lowercased|uppercased].\n\nPatterns should be quoted on the command line.";

const char *gengetopt_args_info_help[] = {
  "      --help       Print help and exit",
  "  -V, --version    Print version and exit",
  "  -h, --hidden     treat dot files normally  (default=off)",
  "  -D, --makedirs   create non-existent directories  (default=off)",
  "\n Group: mode\n  Mode of operation",
  "  -m, --move       move source file to target name",
  "  -x, --copydel    copy source to target, then delete source",
  "  -r, --rename     rename source to target in same directory",
  "  -c, --copy       copy source to target, preserving source permissions",
  "  -o, --overwrite  overwrite target with source, preserving target permissions",
  "  -l, --hardlink   link target name to source file",
  "  -s, --symlink    symlink target name to source file",
  "\n Group: delete\n  How to handle file deletions and overwrites",
  "  -d, --force      perform file deletes and overwrites without confirmation",
  "  -p, --protect    treat file deletes and overwrites as errors",
  "\n Group: erroneous\n  How to handle erroneous actions",
  "  -g, --go         skip any erroneous actions",
  "  -t, --terminate  erroneous actions are treated as errors",
  "\n Group: report\n  Reporting actions",
  "  -v, --verbose    report all actions performed",
  "  -n, --dryrun     only report which actions would be performed",
    0
};

typedef enum {ARG_NO
  , ARG_FLAG
} cmdline_parser_arg_type;

static
void clear_given (struct gengetopt_args_info *args_info);
static
void clear_args (struct gengetopt_args_info *args_info);

static int
cmdline_parser_internal (int argc, char **argv, struct gengetopt_args_info *args_info,
                        struct cmdline_parser_params *params, const char *additional_error);


static char *
gengetopt_strdup (const char *s);

static
void clear_given (struct gengetopt_args_info *args_info)
{
  args_info->help_given = 0 ;
  args_info->version_given = 0 ;
  args_info->hidden_given = 0 ;
  args_info->makedirs_given = 0 ;
  args_info->move_given = 0 ;
  args_info->copydel_given = 0 ;
  args_info->rename_given = 0 ;
  args_info->copy_given = 0 ;
  args_info->overwrite_given = 0 ;
  args_info->hardlink_given = 0 ;
  args_info->symlink_given = 0 ;
  args_info->force_given = 0 ;
  args_info->protect_given = 0 ;
  args_info->go_given = 0 ;
  args_info->terminate_given = 0 ;
  args_info->verbose_given = 0 ;
  args_info->dryrun_given = 0 ;
  args_info->delete_group_counter = 0 ;
  args_info->erroneous_group_counter = 0 ;
  args_info->mode_group_counter = 0 ;
  args_info->report_group_counter = 0 ;
}

static
void clear_args (struct gengetopt_args_info *args_info)
{
  FIX_UNUSED (args_info);
  args_info->hidden_flag = 0;
  args_info->makedirs_flag = 0;
  
}

static
void init_args_info(struct gengetopt_args_info *args_info)
{


  args_info->help_help = gengetopt_args_info_help[0] ;
  args_info->version_help = gengetopt_args_info_help[1] ;
  args_info->hidden_help = gengetopt_args_info_help[2] ;
  args_info->makedirs_help = gengetopt_args_info_help[3] ;
  args_info->move_help = gengetopt_args_info_help[5] ;
  args_info->copydel_help = gengetopt_args_info_help[6] ;
  args_info->rename_help = gengetopt_args_info_help[7] ;
  args_info->copy_help = gengetopt_args_info_help[8] ;
  args_info->overwrite_help = gengetopt_args_info_help[9] ;
  args_info->hardlink_help = gengetopt_args_info_help[10] ;
  args_info->symlink_help = gengetopt_args_info_help[11] ;
  args_info->force_help = gengetopt_args_info_help[13] ;
  args_info->protect_help = gengetopt_args_info_help[14] ;
  args_info->go_help = gengetopt_args_info_help[16] ;
  args_info->terminate_help = gengetopt_args_info_help[17] ;
  args_info->verbose_help = gengetopt_args_info_help[19] ;
  args_info->dryrun_help = gengetopt_args_info_help[20] ;
  
}

void
cmdline_parser_print_version (void)
{
  printf ("%s %s\n",
     (strlen(CMDLINE_PARSER_PACKAGE_NAME) ? CMDLINE_PARSER_PACKAGE_NAME : CMDLINE_PARSER_PACKAGE),
     CMDLINE_PARSER_VERSION);

  if (strlen(gengetopt_args_info_versiontext) > 0)
    printf("\n%s\n", gengetopt_args_info_versiontext);
}

static void print_help_common(void)
{
	size_t len_purpose = strlen(gengetopt_args_info_purpose);
	size_t len_usage = strlen(gengetopt_args_info_usage);

	if (len_usage > 0) {
		printf("%s\n", gengetopt_args_info_usage);
	}
	if (len_purpose > 0) {
		printf("%s\n", gengetopt_args_info_purpose);
	}

	if (len_usage || len_purpose) {
		printf("\n");
	}

	if (strlen(gengetopt_args_info_description) > 0) {
		printf("%s\n\n", gengetopt_args_info_description);
	}
}

void
cmdline_parser_print_help (void)
{
  int i = 0;
  print_help_common();
  while (gengetopt_args_info_help[i])
    printf("%s\n", gengetopt_args_info_help[i++]);
}

void
cmdline_parser_init (struct gengetopt_args_info *args_info)
{
  clear_given (args_info);
  clear_args (args_info);
  init_args_info (args_info);

  args_info->inputs = 0;
  args_info->inputs_num = 0;
}

void
cmdline_parser_params_init(struct cmdline_parser_params *params)
{
  if (params)
    { 
      params->override = 0;
      params->initialize = 1;
      params->check_required = 1;
      params->check_ambiguity = 0;
      params->print_errors = 1;
    }
}

struct cmdline_parser_params *
cmdline_parser_params_create(void)
{
  struct cmdline_parser_params *params = 
    (struct cmdline_parser_params *)malloc(sizeof(struct cmdline_parser_params));
  cmdline_parser_params_init(params);  
  return params;
}



static void
cmdline_parser_release (struct gengetopt_args_info *args_info)
{
  unsigned int i;
  
  
  for (i = 0; i < args_info->inputs_num; ++i)
    free (args_info->inputs [i]);

  if (args_info->inputs_num)
    free (args_info->inputs);

  clear_given (args_info);
}


static void
write_into_file(FILE *outfile, const char *opt, const char *arg, const char *values[])
{
  FIX_UNUSED (values);
  if (arg) {
    fprintf(outfile, "%s=\"%s\"\n", opt, arg);
  } else {
    fprintf(outfile, "%s\n", opt);
  }
}


int
cmdline_parser_dump(FILE *outfile, struct gengetopt_args_info *args_info)
{
  int i = 0;

  if (!outfile)
    {
      fprintf (stderr, "%s: cannot dump options to stream\n", CMDLINE_PARSER_PACKAGE);
      return EXIT_FAILURE;
    }

  if (args_info->help_given)
    write_into_file(outfile, "help", 0, 0 );
  if (args_info->version_given)
    write_into_file(outfile, "version", 0, 0 );
  if (args_info->hidden_given)
    write_into_file(outfile, "hidden", 0, 0 );
  if (args_info->makedirs_given)
    write_into_file(outfile, "makedirs", 0, 0 );
  if (args_info->move_given)
    write_into_file(outfile, "move", 0, 0 );
  if (args_info->copydel_given)
    write_into_file(outfile, "copydel", 0, 0 );
  if (args_info->rename_given)
    write_into_file(outfile, "rename", 0, 0 );
  if (args_info->copy_given)
    write_into_file(outfile, "copy", 0, 0 );
  if (args_info->overwrite_given)
    write_into_file(outfile, "overwrite", 0, 0 );
  if (args_info->hardlink_given)
    write_into_file(outfile, "hardlink", 0, 0 );
  if (args_info->symlink_given)
    write_into_file(outfile, "symlink", 0, 0 );
  if (args_info->force_given)
    write_into_file(outfile, "force", 0, 0 );
  if (args_info->protect_given)
    write_into_file(outfile, "protect", 0, 0 );
  if (args_info->go_given)
    write_into_file(outfile, "go", 0, 0 );
  if (args_info->terminate_given)
    write_into_file(outfile, "terminate", 0, 0 );
  if (args_info->verbose_given)
    write_into_file(outfile, "verbose", 0, 0 );
  if (args_info->dryrun_given)
    write_into_file(outfile, "dryrun", 0, 0 );
  

  i = EXIT_SUCCESS;
  return i;
}

int
cmdline_parser_file_save(const char *filename, struct gengetopt_args_info *args_info)
{
  FILE *outfile;
  int i = 0;

  outfile = fopen(filename, "w");

  if (!outfile)
    {
      fprintf (stderr, "%s: cannot open file for writing: %s\n", CMDLINE_PARSER_PACKAGE, filename);
      return EXIT_FAILURE;
    }

  i = cmdline_parser_dump(outfile, args_info);
  fclose (outfile);

  return i;
}

void
cmdline_parser_free (struct gengetopt_args_info *args_info)
{
  cmdline_parser_release (args_info);
}

/** @brief replacement of strdup, which is not standard */
char *
gengetopt_strdup (const char *s)
{
  char *result = 0;
  if (!s)
    return result;

  result = (char*)malloc(strlen(s) + 1);
  if (result == (char*)0)
    return (char*)0;
  strcpy(result, s);
  return result;
}

static void
reset_group_delete(struct gengetopt_args_info *args_info)
{
  if (! args_info->delete_group_counter)
    return;
  
  args_info->force_given = 0 ;
  args_info->protect_given = 0 ;

  args_info->delete_group_counter = 0;
}

static void
reset_group_erroneous(struct gengetopt_args_info *args_info)
{
  if (! args_info->erroneous_group_counter)
    return;
  
  args_info->go_given = 0 ;
  args_info->terminate_given = 0 ;

  args_info->erroneous_group_counter = 0;
}

static void
reset_group_mode(struct gengetopt_args_info *args_info)
{
  if (! args_info->mode_group_counter)
    return;
  
  args_info->move_given = 0 ;
  args_info->copydel_given = 0 ;
  args_info->rename_given = 0 ;
  args_info->copy_given = 0 ;
  args_info->overwrite_given = 0 ;
  args_info->hardlink_given = 0 ;
  args_info->symlink_given = 0 ;

  args_info->mode_group_counter = 0;
}

static void
reset_group_report(struct gengetopt_args_info *args_info)
{
  if (! args_info->report_group_counter)
    return;
  
  args_info->verbose_given = 0 ;
  args_info->dryrun_given = 0 ;

  args_info->report_group_counter = 0;
}

int
cmdline_parser (int argc, char **argv, struct gengetopt_args_info *args_info)
{
  return cmdline_parser2 (argc, argv, args_info, 0, 1, 1);
}

int
cmdline_parser_ext (int argc, char **argv, struct gengetopt_args_info *args_info,
                   struct cmdline_parser_params *params)
{
  int result;
  result = cmdline_parser_internal (argc, argv, args_info, params, 0);

  if (result == EXIT_FAILURE)
    {
      cmdline_parser_free (args_info);
      exit (EXIT_FAILURE);
    }
  
  return result;
}

int
cmdline_parser2 (int argc, char **argv, struct gengetopt_args_info *args_info, int override, int initialize, int check_required)
{
  int result;
  struct cmdline_parser_params params;
  
  params.override = override;
  params.initialize = initialize;
  params.check_required = check_required;
  params.check_ambiguity = 0;
  params.print_errors = 1;

  result = cmdline_parser_internal (argc, argv, args_info, &params, 0);

  if (result == EXIT_FAILURE)
    {
      cmdline_parser_free (args_info);
      exit (EXIT_FAILURE);
    }
  
  return result;
}

int
cmdline_parser_required (struct gengetopt_args_info *args_info, const char *prog_name)
{
  FIX_UNUSED (args_info);
  FIX_UNUSED (prog_name);
  return EXIT_SUCCESS;
}


static char *package_name = 0;

/**
 * @brief updates an option
 * @param field the generic pointer to the field to update
 * @param orig_field the pointer to the orig field
 * @param field_given the pointer to the number of occurrence of this option
 * @param prev_given the pointer to the number of occurrence already seen
 * @param value the argument for this option (if null no arg was specified)
 * @param possible_values the possible values for this option (if specified)
 * @param default_value the default value (in case the option only accepts fixed values)
 * @param arg_type the type of this option
 * @param check_ambiguity @see cmdline_parser_params.check_ambiguity
 * @param override @see cmdline_parser_params.override
 * @param no_free whether to free a possible previous value
 * @param multiple_option whether this is a multiple option
 * @param long_opt the corresponding long option
 * @param short_opt the corresponding short option (or '-' if none)
 * @param additional_error possible further error specification
 */
static
int update_arg(void *field, char **orig_field,
               unsigned int *field_given, unsigned int *prev_given, 
               char *value, const char *possible_values[],
               const char *default_value,
               cmdline_parser_arg_type arg_type,
               int check_ambiguity, int override,
               int no_free, int multiple_option,
               const char *long_opt, char short_opt,
               const char *additional_error)
{
  char *stop_char = 0;
  const char *val = value;
  int found;
  FIX_UNUSED (field);

  stop_char = 0;
  found = 0;

  if (!multiple_option && prev_given && (*prev_given || (check_ambiguity && *field_given)))
    {
      if (short_opt != '-')
        fprintf (stderr, "%s: `--%s' (`-%c') option given more than once%s\n", 
               package_name, long_opt, short_opt,
               (additional_error ? additional_error : ""));
      else
        fprintf (stderr, "%s: `--%s' option given more than once%s\n", 
               package_name, long_opt,
               (additional_error ? additional_error : ""));
      return 1; /* failure */
    }

  FIX_UNUSED (default_value);
    
  if (field_given && *field_given && ! override)
    return 0;
  if (prev_given)
    (*prev_given)++;
  if (field_given)
    (*field_given)++;
  if (possible_values)
    val = possible_values[found];

  switch(arg_type) {
  case ARG_FLAG:
    *((int *)field) = !*((int *)field);
    break;
  default:
    break;
  };

	FIX_UNUSED(stop_char);
			FIX_UNUSED(val);
	
  /* store the original value */
  switch(arg_type) {
  case ARG_NO:
  case ARG_FLAG:
    break;
  default:
    if (value && orig_field) {
      if (no_free) {
        *orig_field = value;
      } else {
        if (*orig_field)
          free (*orig_field); /* free previous string */
        *orig_field = gengetopt_strdup (value);
      }
    }
  };

  return 0; /* OK */
}


int
cmdline_parser_internal (
  int argc, char **argv, struct gengetopt_args_info *args_info,
                        struct cmdline_parser_params *params, const char *additional_error)
{
  int c;	/* Character of the parsed option.  */

  int error_occurred = 0;
  struct gengetopt_args_info local_args_info;
  
  int override;
  int initialize;
  int check_required;
  int check_ambiguity;
  
  package_name = argv[0];
  
  /* TODO: Why is this here? It is not used anywhere. */
  override = params->override;
  FIX_UNUSED(override);

  initialize = params->initialize;
  check_required = params->check_required;

  /* TODO: Why is this here? It is not used anywhere. */
  check_ambiguity = params->check_ambiguity;
  FIX_UNUSED(check_ambiguity);

  if (initialize)
    cmdline_parser_init (args_info);

  cmdline_parser_init (&local_args_info);

  optarg = 0;
  optind = 0;
  opterr = params->print_errors;
  optopt = '?';

  while (1)
    {
      int option_index = 0;

      static struct option long_options[] = {
        { "help",	0, NULL, 0 },
        { "version",	0, NULL, 'V' },
        { "hidden",	0, NULL, 'h' },
        { "makedirs",	0, NULL, 'D' },
        { "move",	0, NULL, 'm' },
        { "copydel",	0, NULL, 'x' },
        { "rename",	0, NULL, 'r' },
        { "copy",	0, NULL, 'c' },
        { "overwrite",	0, NULL, 'o' },
        { "hardlink",	0, NULL, 'l' },
        { "symlink",	0, NULL, 's' },
        { "force",	0, NULL, 'd' },
        { "protect",	0, NULL, 'p' },
        { "go",	0, NULL, 'g' },
        { "terminate",	0, NULL, 't' },
        { "verbose",	0, NULL, 'v' },
        { "dryrun",	0, NULL, 'n' },
        { 0,  0, 0, 0 }
      };

      c = getopt_long (argc, argv, "VhDmxrcolsdpgtvn", long_options, &option_index);

      if (c == -1) break;	/* Exit from `while (1)' loop.  */

      switch (c)
        {
        case 'V':	/* Print version and exit.  */
          cmdline_parser_print_version ();
          cmdline_parser_free (&local_args_info);
          exit (EXIT_SUCCESS);

        case 'h':	/* treat dot files normally.  */
        
        
          if (update_arg((void *)&(args_info->hidden_flag), 0, &(args_info->hidden_given),
              &(local_args_info.hidden_given), optarg, 0, 0, ARG_FLAG,
              check_ambiguity, override, 1, 0, "hidden", 'h',
              additional_error))
            goto failure;
        
          break;
        case 'D':	/* create non-existent directories.  */
        
        
          if (update_arg((void *)&(args_info->makedirs_flag), 0, &(args_info->makedirs_given),
              &(local_args_info.makedirs_given), optarg, 0, 0, ARG_FLAG,
              check_ambiguity, override, 1, 0, "makedirs", 'D',
              additional_error))
            goto failure;
        
          break;
        case 'm':	/* move source file to target name.  */
        
          if (args_info->mode_group_counter && override)
            reset_group_mode (args_info);
          args_info->mode_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->move_given),
              &(local_args_info.move_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "move", 'm',
              additional_error))
            goto failure;
        
          break;
        case 'x':	/* copy source to target, then delete source.  */
        
          if (args_info->mode_group_counter && override)
            reset_group_mode (args_info);
          args_info->mode_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->copydel_given),
              &(local_args_info.copydel_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "copydel", 'x',
              additional_error))
            goto failure;
        
          break;
        case 'r':	/* rename source to target in same directory.  */
        
          if (args_info->mode_group_counter && override)
            reset_group_mode (args_info);
          args_info->mode_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->rename_given),
              &(local_args_info.rename_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "rename", 'r',
              additional_error))
            goto failure;
        
          break;
        case 'c':	/* copy source to target, preserving source permissions.  */
        
          if (args_info->mode_group_counter && override)
            reset_group_mode (args_info);
          args_info->mode_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->copy_given),
              &(local_args_info.copy_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "copy", 'c',
              additional_error))
            goto failure;
        
          break;
        case 'o':	/* overwrite target with source, preserving target permissions.  */
        
          if (args_info->mode_group_counter && override)
            reset_group_mode (args_info);
          args_info->mode_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->overwrite_given),
              &(local_args_info.overwrite_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "overwrite", 'o',
              additional_error))
            goto failure;
        
          break;
        case 'l':	/* link target name to source file.  */
        
          if (args_info->mode_group_counter && override)
            reset_group_mode (args_info);
          args_info->mode_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->hardlink_given),
              &(local_args_info.hardlink_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "hardlink", 'l',
              additional_error))
            goto failure;
        
          break;
        case 's':	/* symlink target name to source file.  */
        
          if (args_info->mode_group_counter && override)
            reset_group_mode (args_info);
          args_info->mode_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->symlink_given),
              &(local_args_info.symlink_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "symlink", 's',
              additional_error))
            goto failure;
        
          break;
        case 'd':	/* perform file deletes and overwrites without confirmation.  */
        
          if (args_info->delete_group_counter && override)
            reset_group_delete (args_info);
          args_info->delete_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->force_given),
              &(local_args_info.force_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "force", 'd',
              additional_error))
            goto failure;
        
          break;
        case 'p':	/* treat file deletes and overwrites as errors.  */
        
          if (args_info->delete_group_counter && override)
            reset_group_delete (args_info);
          args_info->delete_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->protect_given),
              &(local_args_info.protect_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "protect", 'p',
              additional_error))
            goto failure;
        
          break;
        case 'g':	/* skip any erroneous actions.  */
        
          if (args_info->erroneous_group_counter && override)
            reset_group_erroneous (args_info);
          args_info->erroneous_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->go_given),
              &(local_args_info.go_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "go", 'g',
              additional_error))
            goto failure;
        
          break;
        case 't':	/* erroneous actions are treated as errors.  */
        
          if (args_info->erroneous_group_counter && override)
            reset_group_erroneous (args_info);
          args_info->erroneous_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->terminate_given),
              &(local_args_info.terminate_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "terminate", 't',
              additional_error))
            goto failure;
        
          break;
        case 'v':	/* report all actions performed.  */
        
          if (args_info->report_group_counter && override)
            reset_group_report (args_info);
          args_info->report_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->verbose_given),
              &(local_args_info.verbose_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "verbose", 'v',
              additional_error))
            goto failure;
        
          break;
        case 'n':	/* only report which actions would be performed.  */
        
          if (args_info->report_group_counter && override)
            reset_group_report (args_info);
          args_info->report_group_counter += 1;
        
          if (update_arg( 0 , 
               0 , &(args_info->dryrun_given),
              &(local_args_info.dryrun_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "dryrun", 'n',
              additional_error))
            goto failure;
        
          break;

        case 0:	/* Long option with no short option */
          if (strcmp (long_options[option_index].name, "help") == 0) {
            cmdline_parser_print_help ();
            cmdline_parser_free (&local_args_info);
            exit (EXIT_SUCCESS);
          }

        case '?':	/* Invalid option.  */
          /* `getopt_long' already printed an error message.  */
          goto failure;

        default:	/* bug: option not considered.  */
          fprintf (stderr, "%s: option unknown: %c%s\n", CMDLINE_PARSER_PACKAGE, c, (additional_error ? additional_error : ""));
          abort ();
        } /* switch */
    } /* while */

  if (args_info->delete_group_counter > 1)
    {
      fprintf (stderr, "%s: %d options of group delete were given. At most one is required%s.\n", argv[0], args_info->delete_group_counter, (additional_error ? additional_error : ""));
      error_occurred = 1;
    }
  
  if (args_info->erroneous_group_counter > 1)
    {
      fprintf (stderr, "%s: %d options of group erroneous were given. At most one is required%s.\n", argv[0], args_info->erroneous_group_counter, (additional_error ? additional_error : ""));
      error_occurred = 1;
    }
  
  if (args_info->mode_group_counter > 1)
    {
      fprintf (stderr, "%s: %d options of group mode were given. At most one is required%s.\n", argv[0], args_info->mode_group_counter, (additional_error ? additional_error : ""));
      error_occurred = 1;
    }
  
  if (args_info->report_group_counter > 1)
    {
      fprintf (stderr, "%s: %d options of group report were given. At most one is required%s.\n", argv[0], args_info->report_group_counter, (additional_error ? additional_error : ""));
      error_occurred = 1;
    }
  


	FIX_UNUSED(check_required);

  cmdline_parser_release (&local_args_info);

  if ( error_occurred )
    return (EXIT_FAILURE);

  if (optind < argc)
    {
      int i = 0 ;
      int found_prog_name = 0;
      /* whether program name, i.e., argv[0], is in the remaining args
         (this may happen with some implementations of getopt,
          but surely not with the one included by gengetopt) */

      i = optind;
      while (i < argc)
        if (argv[i++] == argv[0]) {
          found_prog_name = 1;
          break;
        }
      i = 0;

      args_info->inputs_num = argc - optind - found_prog_name;
      args_info->inputs =
        (char **)(malloc ((args_info->inputs_num)*sizeof(char *))) ;
      while (optind < argc)
        if (argv[optind++] != argv[0])
          args_info->inputs[ i++ ] = gengetopt_strdup (argv[optind-1]) ;
    }

  return 0;

failure:
  
  cmdline_parser_release (&local_args_info);
  return (EXIT_FAILURE);
}
/* vim: set ft=c noet ts=8 sts=8 sw=8 tw=80 nojs spell : */
